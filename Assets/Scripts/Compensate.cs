﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Compensate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (transform.parent.hasChanged)
		{
			Vector3 p = transform.parent.localScale;
			Vector3 i = new Vector3(1 / p.x, 1 / p.y, 1 / p.z);
			transform.localScale = i;
			//we add in the parent's y position to allow for different placements along the y axis
			transform.position =
				new Vector3(transform.position.x, transform.parent.position.y + 2.5f + p.y * p.y , transform.position.z);

		}
	}
}
