﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stationary : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (transform.parent.hasChanged)
		{
			Vector3 p = transform.parent.localScale;
			Vector3 i = new Vector3(1 / p.x, 1 / p.y, 1 / p.z);
			transform.localScale = i;
			transform.localPosition =
				new Vector3(transform.localPosition.x, 1 / p.y, transform.localPosition.z);
		}
	}
}
