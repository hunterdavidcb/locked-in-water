﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class InverseCompensate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (transform.parent.hasChanged)
		{
			Vector3 p = transform.parent.localScale;
			//Vector3 i = new Vector3(1 / p.x, 1 / p.y, 1 / p.z);
			p = new Vector3(transform.localScale.x, p.y, transform.localScale.z);
			transform.localScale = p;
			transform.localPosition =
				new Vector3(transform.localPosition.x, p.y / 2, transform.localPosition.z);
		}
	}
}
