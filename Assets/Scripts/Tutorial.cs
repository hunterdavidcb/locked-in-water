﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
	public string[] messages;

	public Button next;
	public GameObject tutPanel;
	public Text display;

	int current = 0;

    // Start is called before the first frame update
    void Start()
    {
		next.onClick.AddListener(OnNextClicked);
		display.text = messages[current];
    }

	private void OnNextClicked()
	{
		current++;

		if (current >= messages.Length)
		{
			tutPanel.SetActive(false);
		}
		else
		{
			display.text = messages[current];
		}
	}
}
