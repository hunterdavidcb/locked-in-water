﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waves : MonoBehaviour
{
	[FMODUnity.EventRef]
	public string waves;
    // Start is called before the first frame update
    void Start()
    {
		FMOD.Studio.EventInstance wave = FMODUnity.RuntimeManager.CreateInstance(waves);
		wave.setVolume(0.05f);
		wave.start();
		//FMODUnity.RuntimeManager.PlayOneShot(waves);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
