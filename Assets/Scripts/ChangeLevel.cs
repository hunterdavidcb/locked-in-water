﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLevel : MonoBehaviour
{
	public Floodgate currentSelection;

	public Floodgate[] all;

	public Material water;
	public Material notWater;

	int current = 0;

	[FMODUnity.EventRef]
	public string drain;

	static bool playingSound;
	FMOD.Studio.EVENT_CALLBACK finished;
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetMouseButton(0))
		{
			if (currentSelection.left != null)
			{
				Vector3 c = currentSelection.transform.localScale;
				c.y -= Time.deltaTime;
				c.y = Mathf.Clamp(c.y, 1f, 5f);
				currentSelection.transform.localScale = c;


				if (c.y > 1f)
				{
					Vector3 l = currentSelection.left.transform.localScale;
					l.y += Time.deltaTime;
					//l.y = Mathf.Clamp(l.y,)
					currentSelection.left.transform.localScale = l;

					currentSelection.leftGen.Make();

					if (!playingSound)
					{
						playingSound = true;
						FMOD.Studio.EventInstance sound = FMODUnity.RuntimeManager.CreateInstance(drain);
						finished = new FMOD.Studio.EVENT_CALLBACK(FinishedCallback);
						sound.setCallback(finished, FMOD.Studio.EVENT_CALLBACK_TYPE.STOPPED);
						sound.start();
						//FMODUnity.RuntimeManager.PlayOneShot(drain);
					}

					AdjustBarriers();

					//if (Mathf.Abs(c.y-l.y) <= 1.5f)
					//{
					//	currentSelection.leftBarrier.enabled = false;
					//	currentSelection.left.GetComponent<Floodgate>().rightBarrier.enabled = false;

					//	Color original = currentSelection.leftBarrier.GetComponent<SpriteRenderer>().color;
					//	Color n = new Color(original.r,original.g,original.b, 0.5f);
					//	currentSelection.leftBarrier.GetComponent<SpriteRenderer>().color = n;


					//	original = currentSelection.left.GetComponent<Floodgate>().
					//		rightBarrier.GetComponent<SpriteRenderer>().color;
					//	n = new Color(original.r, original.g, original.b, 0.5f);
					//	currentSelection.left.GetComponent<Floodgate>().rightBarrier
					//		.GetComponent<SpriteRenderer>().color = n;

					//	//TODO: check the 
					//}
					//else
					//{
					//	currentSelection.leftBarrier.enabled = true;

					//	Color original = currentSelection.leftBarrier.GetComponent<SpriteRenderer>().color;
					//	Color n = new Color(original.r, original.g, original.b, 1f);
					//	currentSelection.leftBarrier.GetComponent<SpriteRenderer>().color = n;

					//	currentSelection.left.GetComponent<Floodgate>().rightBarrier.enabled = true;

					//	original = currentSelection.left.GetComponent<Floodgate>().
					//		rightBarrier.GetComponent<SpriteRenderer>().color;
					//	n = new Color(original.r, original.g, original.b, 1f);
					//	currentSelection.left.GetComponent<Floodgate>().rightBarrier
					//		.GetComponent<SpriteRenderer>().color = n;
					//}
				}
			}
		}

		else if (Input.GetMouseButton(1))
		{
			if (currentSelection.right != null)
			{
				Vector3 c = currentSelection.transform.localScale;
				c.y -= Time.deltaTime;
				c.y = Mathf.Clamp(c.y, 1f, 5f);
				currentSelection.transform.localScale = c;


				if (c.y > 1f)
				{
					Vector3 r = currentSelection.right.transform.localScale;
					r.y += Time.deltaTime;
					currentSelection.right.transform.localScale = r;

					currentSelection.rightGen.Make();

					if (!playingSound)
					{
						playingSound = true;
						FMOD.Studio.EventInstance sound = FMODUnity.RuntimeManager.CreateInstance(drain);
						finished = new FMOD.Studio.EVENT_CALLBACK(FinishedCallback);
						sound.setCallback(finished, FMOD.Studio.EVENT_CALLBACK_TYPE.STOPPED);
						sound.start();
						//FMODUnity.RuntimeManager.PlayOneShot(drain);
					}

					AdjustBarriers();
					//if (Mathf.Abs(c.y - r.y) <= 1.5f)
					//{
					//	currentSelection.rightBarrier.enabled = false;
					//	Color original = currentSelection.rightBarrier.GetComponent<SpriteRenderer>().color;
					//	Color n = new Color(original.r, original.g, original.b, 0.5f);
					//	currentSelection.rightBarrier.GetComponent<SpriteRenderer>().color = n;

					//	currentSelection.right.GetComponent<Floodgate>().leftBarrier.enabled = false;

					//	original = currentSelection.right.GetComponent<Floodgate>()
					//		.leftBarrier.GetComponent<SpriteRenderer>().color;
					//	n = new Color(original.r, original.g, original.b, 0.5f);
					//	currentSelection.right.GetComponent<Floodgate>()
					//		.leftBarrier.GetComponent<SpriteRenderer>().color = n;
					//}
					//else
					//{
					//	currentSelection.rightBarrier.enabled = true;

					//	Color original = currentSelection.rightBarrier.GetComponent<SpriteRenderer>().color;
					//	Color n = new Color(original.r, original.g, original.b, 1f);
					//	currentSelection.rightBarrier.GetComponent<SpriteRenderer>().color = n;

					//	currentSelection.right.GetComponent<Floodgate>().leftBarrier.enabled = true;

					//	original = currentSelection.right.GetComponent<Floodgate>()
					//		.leftBarrier.GetComponent<SpriteRenderer>().color;
					//	n = new Color(original.r, original.g, original.b, 1f);
					//	currentSelection.right.GetComponent<Floodgate>()
					//		.leftBarrier.GetComponent<SpriteRenderer>().color = n;
					//}
				}
			}
		}

		if (Input.mouseScrollDelta.y > 0.2f)
		{
			current++;
			if (current >= all.Length)
			{
				current = 0;
			}

			currentSelection = all[current];
			currentSelection.
				transform.GetChild(0).GetComponent<SpriteRenderer>().material = water;
			foreach (var item in all)
			{
				if (item != currentSelection)
				{
					item.transform.GetChild(0).GetComponent<SpriteRenderer>().material = notWater;
				}
			}
		}

		if (Input.mouseScrollDelta.y < -0.2f)
		{
			current--;
			if (current < 0)
			{
				current = all.Length-1;
			}

			currentSelection = all[current];
			currentSelection.transform.GetChild(0).GetComponent<SpriteRenderer>().material = water;
			foreach (var item in all)
			{
				if (item != currentSelection)
				{
					item.transform.GetChild(0).GetComponent<SpriteRenderer>().material = notWater;
				}
			}
		}
	}

	[AOT.MonoPInvokeCallback(typeof(FMOD.Studio.EVENT_CALLBACK))]
	static FMOD.RESULT FinishedCallback(FMOD.Studio.EVENT_CALLBACK_TYPE type,
		FMOD.Studio.EventInstance instance, System.IntPtr parameterPtr)
	{
		//Debug.Log("finished");
		playingSound = false;

		return FMOD.RESULT.OK;
	}


	void AdjustBarriers()
	{
		GameObject left = all[0].left;

		if (left != null)
		{
			float c = all[0].transform.localScale.y;
			float l = left.transform.localScale.y;

			if (Mathf.Abs(c - l) <= 1.5f)
			{
				all[0].leftBarrier.enabled = false;
				Color original = all[0].leftBarrier.GetComponent<SpriteRenderer>().color;
				Color n = new Color(original.r, original.g, original.b, 0.5f);
				all[0].leftBarrier.GetComponent<SpriteRenderer>().color = n;

				left.GetComponent<Floodgate>().rightBarrier.enabled = false;

				original = left.GetComponent<Floodgate>()
					.rightBarrier.GetComponent<SpriteRenderer>().color;
				n = new Color(original.r, original.g, original.b, 0.5f);
				left.GetComponent<Floodgate>()
					.rightBarrier.GetComponent<SpriteRenderer>().color = n;
			}
			else
			{
				all[0].leftBarrier.enabled = true;
				Color original = all[0].leftBarrier.GetComponent<SpriteRenderer>().color;
				Color n = new Color(original.r, original.g, original.b, 1f);
				all[0].leftBarrier.GetComponent<SpriteRenderer>().color = n;

				left.GetComponent<Floodgate>().rightBarrier.enabled = true;

				original = left.GetComponent<Floodgate>()
					.rightBarrier.GetComponent<SpriteRenderer>().color;
				n = new Color(original.r, original.g, original.b, 1f);
				left.GetComponent<Floodgate>()
					.rightBarrier.GetComponent<SpriteRenderer>().color = n;
			}
		}


		for (int i = 0; i < all.Length ; i++)
		{
			GameObject right = all[i].right;
			if (right != null)
			{
				float c = all[i].transform.localScale.y;
				float r = right.transform.localScale.y;

				if (Mathf.Abs(c - r) <= 1.5f)
				{
					all[i].rightBarrier.enabled = false;
					Color original = all[i].rightBarrier.GetComponent<SpriteRenderer>().color;
					Color n = new Color(original.r, original.g, original.b, 0.5f);
					all[i].rightBarrier.GetComponent<SpriteRenderer>().color = n;

					right.GetComponent<Floodgate>().leftBarrier.enabled = false;

					original = right.GetComponent<Floodgate>()
						.leftBarrier.GetComponent<SpriteRenderer>().color;
					n = new Color(original.r, original.g, original.b, 0.5f);
					right.GetComponent<Floodgate>()
						.leftBarrier.GetComponent<SpriteRenderer>().color = n;
				}
				else
				{
					all[i].rightBarrier.enabled = true;
					Color original = all[i].rightBarrier.GetComponent<SpriteRenderer>().color;
					Color n = new Color(original.r, original.g, original.b, 1f);
					all[i].rightBarrier.GetComponent<SpriteRenderer>().color = n;

					right.GetComponent<Floodgate>().leftBarrier.enabled = true;

					original = right.GetComponent<Floodgate>()
						.leftBarrier.GetComponent<SpriteRenderer>().color;
					n = new Color(original.r, original.g, original.b, 1f);
					right.GetComponent<Floodgate>()
						.leftBarrier.GetComponent<SpriteRenderer>().color = n;
				}
			}
		}

		//foreach (var item in all)
		//{
		//	//GameObject left = item.left;
		//	GameObject right = item.right;

			

		//	if (right != null)
		//	{
		//		float c = item.transform.localScale.y;
		//		float r = right.transform.localScale.y;

		//		if (Mathf.Abs(c - r) <= 1.5f)
		//		{
		//			item.rightBarrier.enabled = false;
		//			Color original = item.rightBarrier.GetComponent<SpriteRenderer>().color;
		//			Color n = new Color(original.r, original.g, original.b, 0.5f);
		//			item.rightBarrier.GetComponent<SpriteRenderer>().color = n;

		//			right.GetComponent<Floodgate>().leftBarrier.enabled = false;

		//			original = right.GetComponent<Floodgate>()
		//				.leftBarrier.GetComponent<SpriteRenderer>().color;
		//			n = new Color(original.r, original.g, original.b, 0.5f);
		//			right.GetComponent<Floodgate>()
		//				.leftBarrier.GetComponent<SpriteRenderer>().color = n;
		//		}
		//		else
		//		{
		//			item.rightBarrier.enabled = true;
		//			Color original = item.rightBarrier.GetComponent<SpriteRenderer>().color;
		//			Color n = new Color(original.r, original.g, original.b, 1f);
		//			item.rightBarrier.GetComponent<SpriteRenderer>().color = n;

		//			right.GetComponent<Floodgate>().leftBarrier.enabled = true;

		//			original = right.GetComponent<Floodgate>()
		//				.leftBarrier.GetComponent<SpriteRenderer>().color;
		//			n = new Color(original.r, original.g, original.b, 1f);
		//			right.GetComponent<Floodgate>()
		//				.leftBarrier.GetComponent<SpriteRenderer>().color = n;
		//		}
		//	}
		//}
	}
}
