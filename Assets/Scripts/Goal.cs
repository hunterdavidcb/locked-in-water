﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
	public delegate void GoalHandler(Goal g);
	public event GoalHandler GoalReached;

	private void Awake()
	{
		FindObjectOfType<EventListener>().RegisterGoal(this);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag.Equals("Player"))
		{
			collision.GetComponent<Move>().enabled = false;
			FindObjectOfType<ChangeLevel>().enabled = false;
			if (GoalReached != null)
			{
				GoalReached(this);
			}
		}
	}
}
