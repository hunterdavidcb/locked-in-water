﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EventListener : MonoBehaviour
{
	public GameObject panel;

	public Text title;
	public Button accept;

	public int levelToLoad;
	bool failed = false;

	private void Awake()
	{
		panel.SetActive(false);
		accept.onClick.AddListener(OnAccepted);
	}

	private void OnAccepted()
	{
		if (!failed)
		{
			SceneManager.LoadScene(levelToLoad);
		}
		else
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
		
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void RegisterShip(Move m)
	{
		m.ShipDestroyed += OnShipDestroyed;
	}

	public void RegisterGoal(Goal g)
	{
		g.GoalReached += OnGoalReached;
	}

	void OnShipDestroyed(Move m)
	{
		Invoke("ShowMenu", 0.5f);
		failed = true;
		title.text = "Too bad. Try again!";
	}

	void OnGoalReached(Goal g)
	{
		Invoke("ShowMenu", 0.5f);
		failed = false;
		title.text = "Great job!";
	}

	void ShowMenu()
	{
		panel.SetActive(true);
	}

}
