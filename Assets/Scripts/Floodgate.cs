﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floodgate : MonoBehaviour
{
	public GameObject right;
	public GameObject left;

	public ParticleGenerator rightGen;
	public ParticleGenerator leftGen;

	public BoxCollider2D leftBarrier;
	public BoxCollider2D rightBarrier;
}
