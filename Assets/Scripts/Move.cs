﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
	new Rigidbody2D rigidbody;
	public Sprite[] sprites;
	public GameObject[] pieces;

	float time;
	int current = 0;

	[FMODUnity.EventRef]
	public string motorOn;
	[FMODUnity.EventRef]
	public string motorOff;
	[FMODUnity.EventRef]
	public string motorRunning;
	[FMODUnity.EventRef]
	public string water;
	[FMODUnity.EventRef]
	public string splash;
	[FMODUnity.EventRef]
	public string crash;

	float t = 0f;

	bool isDown = false;
	static bool isSplashing = false;

	FMOD.Studio.EVENT_CALLBACK finished;
	FMOD.Studio.EventInstance on;
	FMOD.Studio.EventInstance running;
	FMOD.Studio.EventInstance wave;

	public delegate void ShipDamageHandler(Move m);
	public event ShipDamageHandler ShipDestroyed;

	private void Awake()
	{
		rigidbody = GetComponent<Rigidbody2D>();
		FindObjectOfType<EventListener>().RegisterShip(this);
	}
	


	private void Update()
	{

		if (Input.GetButtonDown("Horizontal"))
		{
			isDown = true;
			//Debug.Log("down");
			OnMovementStarted();
		}

		if (Input.GetButtonUp("Horizontal"))
		{
			isDown = false;
			//Debug.Log("up");
			OnMovementFinished();
		}
	}

	// Update is called once per frame
	void LateUpdate()
    {
		float h = Input.GetAxis("Horizontal");

		if (Mathf.Abs(h) > 0 && isDown)
		{
			//Debug.Log("held");
		}

		

		if (h > 0f)
		{
			rigidbody.velocity = new Vector2(2f, rigidbody.velocity.y);
			transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 0f, transform.rotation.eulerAngles.z);
			
		}

		if (h < 0)
		{
			rigidbody.velocity = new Vector2(-2f, rigidbody.velocity.y);
			transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 180f, transform.rotation.eulerAngles.z);
		}

		if (Mathf.Abs(rigidbody.velocity.y) > 5f && !isSplashing)
		{
			isSplashing = true;
			FMOD.Studio.EventInstance sound = FMODUnity.RuntimeManager.CreateInstance(splash);
			finished = new FMOD.Studio.EVENT_CALLBACK(FinishedCallback);
			sound.setVolume(.5f);
			sound.setCallback(finished, FMOD.Studio.EVENT_CALLBACK_TYPE.STOPPED);
			sound.start();
		}

		if (Mathf.Abs(rigidbody.velocity.y) > 10f)
		{
			if (Time.time - time > 1f)
			{
				Debug.Log("Big!!!");
				time = Time.time;
				Damage();
			}
			
		}

		rigidbody.velocity = new Vector2(rigidbody.velocity.x, Mathf.Clamp(rigidbody.velocity.y, -10f, 10f));

		rigidbody.angularVelocity = Mathf.Clamp(rigidbody.angularVelocity, -10f, 10f);
    }

	void OnMovementStarted()
	{
		on = FMODUnity.RuntimeManager.CreateInstance(motorOn);
		on.start();
		int i;
		FMODUnity.RuntimeManager.GetEventDescription(motorOn).getLength(out i);
		t = (float)(i-200) / 1000f;
		//Debug.Log(t);
		Invoke("Running", t);
		wave = FMODUnity.RuntimeManager.CreateInstance(water);
		wave.start();
		//FMODUnity.RuntimeManager.PlayOneShot(water);
	}

	void Running()
	{
		//Debug.Log("called");
		running = FMODUnity.RuntimeManager.CreateInstance(motorRunning);
		running.start();
		//FMODUnity.RuntimeManager.PlayOneShot(motorRunning);
	}


	void MovementContinued()
	{

	}

	void OnMovementFinished()
	{
		CancelInvoke("Running");
		//Debug.Log("finished");
		on.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
		running.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
		wave.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
		//FMODUnity.RuntimeManager.
		FMODUnity.RuntimeManager.PlayOneShot(motorOff);
		//Debug.Log("played");
	}

	void Damage()
	{
		//Debug.Log("damaging");
		current++;

		if (current == sprites.Length)
		{
			Destroy();
		}
		else
		{
			GetComponent<SpriteRenderer>().sprite = sprites[current];
		}
	}

	void Destroy()
	{
		foreach (var item in pieces)
		{
			Instantiate(item, transform.position, transform.rotation);
		}

		if (ShipDestroyed != null)
		{
			ShipDestroyed(this);
		}

		Destroy(gameObject);
	}

	[AOT.MonoPInvokeCallback(typeof(FMOD.Studio.EVENT_CALLBACK))]
	static FMOD.RESULT FinishedCallback(FMOD.Studio.EVENT_CALLBACK_TYPE type,
		FMOD.Studio.EventInstance instance, System.IntPtr parameterPtr)
	{
		//Debug.Log("finished");
		isSplashing = false;

		return FMOD.RESULT.OK;
	}

	private void OnDisable()
	{
		on.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
		running.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
		wave.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
	}
}
