﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Metaballs" {
	Properties{


		_MyColor("Some Color", Color) = (1,1,1,1)
		_MainTex("Texture", 2D) = "white" { }

	_botmcut("bottom cutoff", Range(0,1)) = 0.1
		_topcut("top cutoff", Range(0,4)) = 0.8
		_constant("curvature constant", Range(0,5)) = 1
	}
		SubShader{
		Tags{ "Queue" = "Transparent" }
		Pass{
		Blend SrcAlpha OneMinusSrcAlpha
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag   
#include "UnityCG.cginc"    
		float4 _MyColor;
	float4 _Color;
	sampler2D _MainTex;
	float _botmcut,_topcut,_constant;

	struct v2f {
		float4  pos : SV_POSITION;
		float2  uv : TEXCOORD0;
	};
	float4 _MainTex_ST;

	v2f vert(appdata_base v) {
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
		return o;
	}

	half4 frag(v2f i) : COLOR{
		half4 texcol,finalColor;
	texcol = tex2D(_MainTex, i.uv);
	//finalColor=_Color*texcol;
	finalColor = _MyColor;
	clip(_topcut - texcol.a );
	if (texcol.a<_botmcut)
	{
		//finalColor.a = 0;
	}
	else if ((texcol.a>_topcut))
	{
		//finalColor.a = 0;
	}
	else
	{
		float r = _topcut - _botmcut;
		float xpos = _topcut - texcol.a;

		finalColor.a = .9 - (_botmcut + sqrt((r*r) - (xpos*xpos))) / _constant;
	}
	return finalColor;
	}
		ENDCG

	}
	}
		Fallback "VertexLit"
}