﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
	public Transform target;
	float moveTime = .5f;

	Vector3 v = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (target != null)
		{
			Vector3 t = new Vector3(target.position.x, transform.position.y, transform.position.z);
			transform.position = Vector3.SmoothDamp(transform.position, t, ref v, moveTime);
			//if (Mathf.Abs(transform.position.x - target.position.x) > .01f)
			//{
			//	if (!reset)
			//	{
			//		reset = true;
			//		cTime = 0;
			//	}

			//	cTime += Time.deltaTime;
			//	//Debug.Log(cTime);

			//	float x = CosineInterpolate(transform.position.x,
			//	target.position.x, cTime / moveTime);
			//	if (Mathf.Abs(x - target.position.x) < 0.01)
			//	{
			//		x = target.position.x;
			//	}
			//	transform.position = new Vector3(x, transform.position.y, transform.position.z);


			//}
			//else
			//{
			//	//Debug.Log(reset);
			//	reset = false;
			//}
			
		}
    }

	float CosineInterpolate(float  y1, float y2, float mu)
	{
		float mu2;

		mu2 = (1 - Mathf.Cos(mu * Mathf.PI)) / 2;
		return (y1 * (1 - mu2) + y2 * mu2);
	}
}
