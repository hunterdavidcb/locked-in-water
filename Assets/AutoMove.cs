﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMove : MonoBehaviour
{
	new Rigidbody2D rigidbody;


	[FMODUnity.EventRef]
	public string motorOn;
	[FMODUnity.EventRef]
	public string motorOff;
	[FMODUnity.EventRef]
	public string motorRunning;
	[FMODUnity.EventRef]
	public string water;

	float time;
	float t = 0f;

	bool isDown = false;

	FMOD.Studio.EVENT_CALLBACK finished;
	FMOD.Studio.EventInstance on;
	FMOD.Studio.EventInstance running;
	FMOD.Studio.EventInstance wave;

	public Transform left;
	public Transform right;

	bool isRight = true;

	private void Awake()
	{
		rigidbody = GetComponent<Rigidbody2D>();
	}
	private void Update()
	{
		//TODO: use these to handle horizontal one time events
		if (!isDown)
		{
			isDown = true;
			//Debug.Log("down");
			OnMovementStarted();
		}

		//if (Input.GetButtonUp("Horizontal"))
		//{
		//	isDown = false;
		//	//Debug.Log("up");
		//	OnMovementFinished();
		//}
	}

	// Update is called once per frame
	void LateUpdate()
	{
		float h = isRight? 1f : -1f;

		if (Mathf.Abs(h) > 0 && isDown)
		{
			//Debug.Log("held");
		}



		if (h > 0f)
		{
			rigidbody.velocity = new Vector2(2f, rigidbody.velocity.y);
			transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 0f, transform.rotation.eulerAngles.z);

		}

		if (h < 0)
		{
			rigidbody.velocity = new Vector2(-2f, rigidbody.velocity.y);
			transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 180f, transform.rotation.eulerAngles.z);
		}


		rigidbody.velocity = new Vector2(rigidbody.velocity.x, Mathf.Clamp(rigidbody.velocity.y, -10f, 10f));

		rigidbody.angularVelocity = Mathf.Clamp(rigidbody.angularVelocity, -10f, 10f);

		if (isRight && Vector3.Distance(transform.position,right.position) < .5f)
		{
			isRight = !isRight;
		}

		if (!isRight && Vector3.Distance(transform.position, left.position) < .5f)
		{
			isRight = !isRight;
		}
	}

	void OnMovementStarted()
	{
		on = FMODUnity.RuntimeManager.CreateInstance(motorOn);
		on.start();
		int i;
		FMODUnity.RuntimeManager.GetEventDescription(motorOn).getLength(out i);
		t = (float)(i - 200) / 1000f;
		//Debug.Log(t);
		Invoke("Running", t);
		wave = FMODUnity.RuntimeManager.CreateInstance(water);
		wave.start();
		//FMODUnity.RuntimeManager.PlayOneShot(water);
	}

	void Running()
	{
		//Debug.Log("called");
		running = FMODUnity.RuntimeManager.CreateInstance(motorRunning);
		running.setVolume(0.5f);
		running.start();
		//FMODUnity.RuntimeManager.PlayOneShot(motorRunning);
	}


	void MovementContinued()
	{

	}

	void OnMovementFinished()
	{
		CancelInvoke("Running");
		//Debug.Log("finished");
		on.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
		running.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
		wave.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
		//FMODUnity.RuntimeManager.
		FMODUnity.RuntimeManager.PlayOneShot(motorOff);
		//Debug.Log("played");
	}
}
